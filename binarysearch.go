package main 

import (
	"fmt"
)

func BinarySearch(key int, order_array []int) int{
	length :=len(order_array)
	if length<=0{
		return -1
	}
	end_index :=length -1
	start_index :=end_index/2
	for{
		if order_array[start_index]==key{
			return start_index
		}else if order_array[start_index]>key{
			end_index = start_index
			start_index = start_index/2
		}else if order_array[start_index]<key{
		    if (end_index-start_index)%2==0{
		    	start_index = start_index + (end_index-start_index)/2
		    }else{
		    	start_index = start_index + (end_index-start_index)/2+1
		    }
		}
		
		if start_index == end_index && order_array[start_index]!=key{
			return -1
		}
	}
	
}
func main() {

	order_list :=[]int{1,2,3,4,5,6,7,8}
	//8
	fmt.Printf("key is %d,order_list is [1,2,3,4,5,6,7,8]\n",8)
	fmt.Printf("this result is %d\n",BinarySearch(8,order_list))
	
	//6
	fmt.Printf("key is %d,order_list is [1,2,3,4,5,6,7,8]\n",6)
	fmt.Printf("this result is %d\n",BinarySearch(6,order_list))
	
	//1
	fmt.Printf("key is %d,order_list is [1,2,3,4,5,6,7,8]\n",1)
	fmt.Printf("this result is %d\n",BinarySearch(1,order_list))

    //0
	fmt.Printf("key is %d,order_list is [1,2,3,4,5,6,7,8]\n",0)
	fmt.Printf("this result is %d\n",BinarySearch(0,order_list))
	
	//9
	fmt.Printf("key is %d,order_list is [1,2,3,4,5,6,7,8]\n",9)
	fmt.Printf("this result is %d\n",BinarySearch(9,order_list))
}

