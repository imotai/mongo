#!/usr/bin/env python
# -*- coding:utf-8 -*-
#测试文档集合
# wiki http://en.wikipedia.org/wiki/Okapi_BM25
import math
test_documents = [{"content":"stay foolish and hungry",
                   "tf":{"stay":1,"foolish":1,"hungry":1}},
		          {"content":"Now is better than never",
	               "tf":{"now":1,"better":1,"never":1}},
		          {"content":"Beautiful is better than ugly",
		           "tf":{"beautilful":1,"better":1,"ugly":1}},
		          {"content":"flat is better than nested",
		           "tf":{"flat":1,"better":1,"nested":1}}]
class BMRank(object):
    def __init__(self,docs,
		      parameter_k=1.5,
                      parameter_b=0.75):
	self.docs = docs
	self.parameter_k = parameter_k
	self.parameter_b = parameter_b
	self.avg_doc_length = self._avg_doc_length()
    def _caculate_idf(self,term):
    	total_doc_count = len(self.docs)
    	contain_term_doc_count = 0
    	for document in self.docs:
            if term not in document["tf"]:
	    	continue
	    contain_term_doc_count+=1
    	idf_before_log = (float(total_doc_count) - float(contain_term_doc_count)+0.5)\
		    /(float(contain_term_doc_count)+0.5)
    	return math.log(idf_before_log,2)
    def _caculate_term_score(self,term,document):
	if term not in document["tf"]:
		return 0
        idf = self._caculate_idf(term)
	tf = document["tf"][term]

        term_score = idf * (tf*self.parameter_k)/\
		     (idf+self.parameter_k*(1-self.parameter_b+\
		     self.parameter_b*len(document["tf"])/self.avg_doc_length))
	return term_score
    def _avg_doc_length(self):
	total_doc_count = len(self.docs)
	total_term_count = 0
	for document in self.docs:
	    total_term_count += len(document["tf"])
	return float(total_doc_count)/float(total_term_count)
    def caculate_score(self,query):
	query_terms = query.split()
	score_dict ={}
	for index,document in enumerate(self.docs):
            document_score = 0
	    for term in query_terms:
                document_score+=self._caculate_term_score(term,document)
	    score_dict[index] = document_score
	return score_dict

if __name__=="__main__":
    test_bm25 = BMRank(test_documents)
    print test_bm25.caculate_score("beautilful or ugly")



    

