package experiment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


public class Thread2 {

	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	public void doTransaction(){
	    try {
	    	Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager
			      .getConnection("jdbc:mysql://localhost:3306/tx_test?"
			          + "user=root");
			connect.setAutoCommit(false);
			preparedStatement = connect
			      .prepareStatement("insert into  COMMENTS values (?, ?, ?)");
			preparedStatement.setInt(1, 2);
			preparedStatement.setString(2, "TestEmail");
			preparedStatement.setString(3, "TestWebpage");
			preparedStatement.executeUpdate();
			Thread.sleep(10*60*1000);
		    connect.commit();
		    connect.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static void main(String[] args) {
		
		Thread2 t2 = new Thread2();
		t2.doTransaction();
	}

}
