#! -*- coding:utf-8 -*-
'''
Created on 2013-9-19
@author: imotai
用k临近算法实现识别手写数字
1、下载数据，从http://archive.ics.uci.edu/ml/datasets/Semeion+Handwritten+Digit，下载测试数据
2、数据抽取
3、随即针对每个数字，随机抽取一定量数据作为sample
4、测试，随机抽去数据测试

'''
from knn import classify
import numpy
import random
import urllib
class Recognition(object):
    def __init__(self,k=1,
                      digit_sample_count=3):
        self.digit_dict = {}
        self.k = k
        self.digit_sample_count = digit_sample_count
    def _load_data(self,fd):
        self.digit_dict = {}
        for line in fd.readlines():
            digit,line_vector = self._line_extractor(line)
            if digit == -1:
                continue
            if digit not in self.digit_dict:
                self.digit_dict[digit] = []
            self.digit_dict[digit].append(line_vector)
    def file_load_data(self,file_path):
        fd = open(file_path,'r')
        self._load_data(fd)
        fd.close()
    def http_load_data(self,url):
        fd = urllib.urlopen(url)
        self._load_data(fd)
        fd.close()
    def _line_extractor(self,line):
        if not line:
            return -1,None
        line_parts = line.split()
        if len(line_parts) != 266:
            return -1,None
        line_vector = numpy.zeros(256)
        for i in range(256):
            #测试数据为16*16矩阵图，这里将其转化为256的向量
            line_vector[i] = float(line_parts[i])
        digit = -1
        for i in range(10):
            if int(line_parts[256+i]) == 1:
                digit = i
                break
        return digit,line_vector
    def _random_sample(self):
        all_data = [] 
        all_label = []
        #生成一个样本矩阵，以及每一行对应的数字
        for key in self.digit_dict:
            all_sample_count = len(self.digit_dict[key])
            if self.digit_sample_count >all_sample_count:
                for line_vertor in self.digit_dict[key]:
                    all_data.append(line_vertor.tolist())
                    all_label.append(key)
                continue
            sample_indexs = random.sample(range(all_sample_count),
                                          self.digit_sample_count)
            for index in sample_indexs:
                all_data.append(self.digit_dict[key][index].tolist())
                all_label.append(key)
        return all_data,all_label   
    def get_random_test_sample(self):
        digit = random.randint(0, 9)
        digit_data = self.digit_dict[digit]
        index = random.sample(range(len(digit_data)),1)[0]
        return digit,digit_data[index]
    def recognise(self,test_vector):
        all_data,all_label = self._random_sample()
        digit = classify(test_vector,numpy.array(all_data),all_label,self.k)
        return digit
        
if __name__ == '__main__':
    recognition = Recognition(digit_sample_count=50)
    recognition.http_load_data("https://bitbucket.org/imotai/mongo/raw/d63802bcf05ab10421365e37464bdb8c9a69a632/handwrite/sample/semeion.data")
    digit,digit_data = recognition.get_random_test_sample()
    print "the real digit is %d"%digit
    recon_digit = recognition.recognise(digit_data.tolist())
    print "the recognised digit is %d"%recon_digit
    