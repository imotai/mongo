package main 

import (
	"fmt"
)
//if使用
func test_if(){
	if 1>2{
		fmt.Println("eoor")
	}else{
		fmt.Println("yeah")
	}
}
// 数组遍历
func test_list(){
	list :=[]string{"a","b","c"}
	for k,v:=range list{
		fmt.Printf("key is %d,value is %s\n",k,v)
	}
}
// 字符串遍历
func test_range_string(){
	hello :="hello world"
	for k,v :=range hello{
		//字符串变成char
		fmt.Printf("key is %d,vlaue is %c\n",k,v)
	}
}
//函数返回
func test_return() string {
	
	return "hello world"
}
//参数传入
func test_parameter(hello string) string{
	return hello
}
//指针传入
func test_pointer(hello *int){
    *hello = 3
} 
func main() {
	test_if()
	test_list()
	test_range_string()
	var hello string = "hello world"
	fmt.Println(hello)
	fmt.Println(test_return())
	fmt.Println(test_parameter("hello world"))
	hello_int :=5
	test_pointer(&hello_int)
	fmt.Printf("the change int is %d\n",hello_int)
}

