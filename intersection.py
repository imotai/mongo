#! -*- coding:utf-8 -*-




def intersection(ordered_list1,ordered_list2):

    index1 = 0
    index2 = 0
    result = []
    while index1<len(ordered_list1) and index2<len(ordered_list2):
        if ordered_list1[index1] == ordered_list2[index2]:
            result.append(ordered_list1[index1])
            index1 += 1
            index2 += 1
        elif ordered_list1[index1] < ordered_list2[index2]:
            index1 += 1
        else:
            index2 += 1
    return result
if __name__=='__main__':
    ordered1 = [1,1,2,3,4,5,6]
    ordered2 = [1,1,3,6,7]
    print 'input is %s and %s'%(str(ordered1),str(ordered2))
    print 'result is %s'%str(intersection(ordered1,ordered2))



