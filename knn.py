# -*- coding:utf-8 -*-
#数据处理流程
#1、准备数据
#2、归一化处理
#3、分类处理
#4、样本测试
from numpy import array,tile
import operator

def create_dataset():
    '''
    this is just sample data
    '''
    group = array([
        [1.0,1.1],
        [1.0,1.0],
        [0,0],
        [0,0.1]
        ])
    labels = ['A','A','B','B']
    return group,labels
def classify(inx,sampleset,labels,k):
    sample_size = sampleset.shape[0]
    #求inx到各个测试数据距离
    diff_matrix = tile(inx,(sample_size,1)) - sampleset
    sq_diff_matrix = diff_matrix**2
    sq_distance = sq_diff_matrix.sum(axis=1)
    #求平方根
    distance = sq_distance**0.5
    #排序
    sorted_distance = distance.argsort()
    class_count = {}
    for i in range(k):
        vote_label = labels[sorted_distance[i]]
        class_count[vote_label]=class_count.get(vote_label,0)+1
    sorted_class_count = sorted(
            class_count.iteritems(),
            key=operator.itemgetter(1),
            reverse=True)
    return sorted_class_count[0][0]

if __name__=='__main__':
    group,labels = create_dataset()
    print classify([0,0],group,labels,3)

