#!/usr/bin/python
#encoding:utf-8

def min_match_seg(query,doc):

	"""
	文档中依次有N个词，现在有一个依次包含M个词的检索串，要求在文档中找到一个包含最少词数的连续片段，使得检索串里的M个词依序出现在这个片段中（当然，可以不连续）
	"""
	#建立词索引
	token_dict = {}
	token_index = 0
	for token in doc:
		if not token_dict.has_key(token):
			token_dict[token] = []
		token_dict[token].append(token_index)
		token_index=token_index+1
	all_result= []
	current_first_location = -1
	for token_location in token_dict[query[0]]:
		result = []
		current_token_location =current_first_location
		for query_token in query:
			for location in token_dict[query_token]:
				if location>current_token_location:
					result.append(location)
					current_token_location = location
					break
		current_first_location = result[0]
		all_result.append(result[-1] - result[0])
		print ' '.join(doc[result[0]:result[-1]+1])
	all_result = sorted(all_result)
	print all_result[0]

if __name__=='__main__':
	query = ['what','is','name']
	doc=['what','what','is','what','your','name','what','a','fuck','is','your','name']
	min_match_seg(query,doc)



