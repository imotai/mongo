#-*- coding:utf-8 -*-
'''
Created on 2013-7-13

@author: innerp
'''
import jieba
import math
def train_classifier(docs,clazz,all_tokens_set_count,all_doc_count):
    #在所有样本中 文档属于clazz类型概率
    p_doc_clazz = float(len(docs))/float(all_doc_count)
    #clazz所有词数
    tokens_clazz_count = 0
    #统计每个token在clazz中词频
    tokens_clazz_dict={}
    for doc in docs:
        tokens_clazz_count +=len(doc)
        for token in doc:
            if not tokens_clazz_dict.has_key(token):
                tokens_clazz_dict[token] = 0
            tokens_clazz_dict[token]+=1
    p_tokens_dict={}
    for key in tokens_clazz_dict.keys():
        #计算每个token在clazz中出现的条件概率
        p_tokens_dict[key] = float(tokens_clazz_dict[key])/\
        float(all_tokens_set_count+tokens_clazz_count)
    return p_doc_clazz,p_tokens_dict

def train_all_samples(samples):
    all_token=[]
    all_doc_count=0
    clazz_docs={}
    classifier={}
    for clazz in samples.keys():
        docs = []
        for text in samples[clazz]:
            doc_tokens = list(text2tokes(text))
            all_token.extend(doc_tokens)
            docs.append(doc_tokens)
            all_doc_count+=1
        clazz_docs[clazz]=docs
    all_token_set_count=len(set(all_token))
    for clazz in clazz_docs.keys():
        p_doc_clazz,p_tokens_dict = train_classifier(clazz_docs[clazz],\
                                                     clazz,all_token_set_count,all_doc_count)
        classifier[clazz] =(p_doc_clazz,p_tokens_dict)
    return classifier
def text2tokes(text):
    token_list = jieba.cut(text, cut_all=False)
    return token_list
def classify(text,classifier):
    token_list = list(text2tokes(text))
    classify_result= []
    for clazz in classifier.keys():
        (p_doc_clazz,p_tokens_dict) = classifier[clazz]
        p_clazz=math.log(p_doc_clazz)
        for token in token_list:
            if not p_tokens_dict.has_key(token):
                continue
            p_clazz+=math.log(p_tokens_dict[token])
        classify_result.append((clazz,p_clazz))
    return classify_result
    