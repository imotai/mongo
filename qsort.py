#!/usr/bin/env python
# -*- coding:utf-8 -*-

def qsort(l):
    if len(l)<=0:return l
    return qsort([x for x in l if x<l[0]])+[x for x in l if x==l[0]]+qsort([x for x in l if x>l[0]])



if __name__=='__main__':
    sample = [3,6,4,1,-9]
    result = qsort(sample)
    print result
