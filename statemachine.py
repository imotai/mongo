# -*- coding:utf-8 -*-
#使用有限状态自动机识别字符串是否包含某个字串
#求字符串是否含有abc这样的字串
class State(object):
    def __init__(self,
                 func,
                 key,
                 is_end=False):
        #根据函数输出和state字典
        self.next_state = {}
        self.func = func
        self.is_end = is_end
        self._key = key
    def _get_key(self):
        return self._key
    def _set_key(self,
                 key):
        self._key = key
    def _del_key(self):
        del self._key
    def add_next_state(self,
                       state):
        self.next_state[state.key] = state

    def end(self):
        return self.is_end
    def do_judge(self,data_input,index):
        print 'run state %s with index %d'%(self._key,index)
        output,index,state_key = self.func(data_input,index)
        if state_key and index !=-1 and not self.is_end and self.next_state.has_key(state_key):
            next_state = self.next_state[state_key]
            return next_state.do_judge(output,index)
        return index
    key = property(_get_key,_set_key,_del_key)


def func0(data_input,index):
    if index>=len(data_input):
        return data_input,-1,None
    if data_input[index]=='a':
        return data_input,index+1,'func1'
    return data_input,index+1,'func0'


def func1(data_input,index):
    if index>=len(data_input):
        return data_input,-1,None
    if data_input[index]=='b':
        return data_input,index+1,'func2'
    return data_input,index+1,'func0'
def func2(data_input,index):
    if index>=len(data_input):
        return data_input,-1,None
    if data_input[index] =='c':
        return data_input,index,'func3'
    return data_input,index+1,'func0'
def func3(data_input,index):
    return data_input,index,None

def main():
    #组装状态机
    state0 = State(func0,'func0')
    state1 = State(func1,'func1')
    state2 = State(func2,'func2')
    state3 = State(func3,'func3',is_end=True)
    #state0的下一个状态可能是state1和它自己
    state0.add_next_state(state1)
    state0.add_next_state(state0)
    state1.add_next_state(state2)
    state1.add_next_state(state0)
    state2.add_next_state(state3)
    state2.add_next_state(state0)
    test_str = 'adasdasdabcddddd'
    #测试
    index_start = state0.do_judge(test_str,0)
    print 'abc is end %d in %s '%(index_start,test_str)

if __name__ == '__main__':
    main()




