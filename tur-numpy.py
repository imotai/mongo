#! -*- coding:utf-8 -*-
# http://wiki.scipy.org/Tentative_NumPy_Tutorial
#上面连接讲的很详细
from numpy import array

mm = array((1,1,1))
#数组元素获取
print mm[1]#1
pp = array((1,2,3))
#数组相加
print mm+pp#[2,3,4]
#数组与标量相乘
print pp*2#[2,4,6]
#数组元素求平方
print pp**2#[1,4,9]
#数组相乘
print pp*mm#[1,2,3]
#多维数组
jj = array([[1,1,1],[1,2,3]])
print jj[0] #[1,1,1]
print jj[0][0]#1
print jj[0,0]#1
from numpy import mat,matrix
#mat是matrix的缩写
ss = mat([1,2,3])
print ss
print ss[0,0]
